<?php
class Bombero extends CI_Model {

  public function __construct() {
      parent::__construct();
  }



  function getAll()
  {
    $listMovie=
    $this->db->get("contador");
    if($listMovie->num_rows()>0){
        return $listMovie->result();
    }else{
        return false;
    }
  }
  // *********************** ACTIVIDAD 3 ****************************
  // FUNCION 1: actividad 3
  public function getByYear($year)
  {
      $sql = "select YEAR(fecha_con) AS Anio, MONTHNAME(fecha_con) AS Mes, COUNT(*) AS TotalVisitas
      FROM contador
      WHERE YEAR(fecha_con) = '$year'
      GROUP BY YEAR(fecha_con), MONTH(fecha_con)
      ORDER BY YEAR(fecha_con), MONTH(fecha_con)";
      $result = $this->db->query($sql);

      if ($result->num_rows() > 0) {
          return $result->result(); // Devolver todos los resultados
      } else {
          return []; // Devolver un arreglo vacío si no hay resultados
      }
  }

  //INDICADOR 1: actividad 3
  function getByTotalVisit20()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2020;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }

  //INDICADOR 2: actividad 3
  function getByTotalVisit21()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2021;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }

  //INDICADOR 3: actividad 3
  function getByTotalVisit22()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2022;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }

  //INDICADOR 4: actividad 3
  function getByTotalVisit23()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2023;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }
  // *********************** FIN ACTIVIDAD 3 ****************************


  // *********************** ACTIVIDAD 4 ********************************
  //LIZ
  // *********************** FIN ACTIVIDAD 4 ****************************


  // *********************** ACTIVIDAD 5 ********************************
  //CINTIA
  // *********************** FIN ACTIVIDAD 5 ****************************


  // *********************** ACTIVIDAD 6 ********************************
  //angel
  // *********************** FIN ACTIVIDAD 6 ****************************


}//The class end

