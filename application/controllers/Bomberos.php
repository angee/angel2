<?php
class Bomberos  extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Bombero');

    }


    public function index()
	{
        // *********************** ACTIVIDAD 3 ****************************
        // Indicadores
        $data["List2020"] = $this->Bombero->getByTotalVisit20();
        $data["List2021"] = $this->Bombero->getByTotalVisit21();
        $data["List2022"] = $this->Bombero->getByTotalVisit22();
        $data["List2023"] = $this->Bombero->getByTotalVisit23();
        // Graficas
        $data["dataForYear2020"] = $this->Bombero->getByYear(2020);
        $data['dataForYear2021'] = $this->Bombero->getByYear(2021);
        $data['dataForYear2022'] = $this->Bombero->getByYear(2022);
        $data['dataForYear2023'] = $this->Bombero->getByYear(2023);

        // *********************** ACTIVIDAD 4 ****************************
        //codigo liz

        // *********************** ACTIVIDAD 5 ****************************
        //codigo cintia

        // *********************** ACTIVIDAD 6 ****************************
        //codigo angel
		$this->load->view('header');
		$this->load->view('bomberos/index', $data);
        $this->load->view('footer');
	}

}
?>
