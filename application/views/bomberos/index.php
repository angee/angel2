
<!-- ******************************************* CANVA ACTIVIDAD 3 ********************************************* -->
<div class="container">
  <h3><center>TOTAL VISITS BY MONTH</center></h3>
  <h5>Actividad 3</h5>
</div>
<div class="row">
        <!-- Año 2020 -->
        <div class="col-md-3">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2020 -->
                    <div class="card-body" style="background-color: #12eeda; height: 90px;">
                        <p class="card-text">YEAR 2020</p>
                        <h5 class="card-title">
                            <?php echo $List2020; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2020 -->
                    <canvas id="lineChart2020" width="300" height="300"></canvas>
                </div>
            </div>
        </div>

        <!-- Año 2021 -->
        <div class="col-md-3 mb-7">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2021 -->
                    <div class="card-body" style="background-color: #43ea43; height: 90px;">
                        <p class="card-text">YEAR 2021</p>
                        <h5 class="card-title">
                            <?php echo $List2021; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2021 -->
                    <canvas id="lineChart2021" width="200" height="200"></canvas>
                </div>
            </div>
        </div>

        <!-- Año 2022 -->
        <div class="col-md-3">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2022 -->
                    <div class="card-body" style="background-color: #727cc0; height: 90px;">
                        <p class="card-text">YEAR 2022</p>
                        <h5 class="card-title">
                            <?php echo $List2022; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2022 -->
                    <canvas id="lineChart2022" width="200" height="200"></canvas>
                </div>
            </div>
        </div>

        <!-- Año 2023 -->
        <div class="col-md-3">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2023 -->
                    <div class="card-body" style="background-color: #db3b3e; height: 90px;">
                        <p class="card-text">YEAR 2023</p>
                        <h5 class="card-title">
                            <?php echo $List2023; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2023 -->
                    <canvas id="lineChart2023" width="200" height="200"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ******************************************* CANVA ACTIVIDAD 4 ********************************************* -->
<!-- LIZ -->

<!-- ******************************************* CANVA ACTIVIDAD 5 ********************************************* -->
<!-- CINTIA -->

<!-- ******************************************* CANVA ACTIVIDAD 6 ********************************************* -->
<!-- ANGEL -->



<!-- ****************************************** SCRIPT ACTIVIDAD 3 *******************************************-->
<!-- 2020 -->
<script>
  <?php if ($dataForYear2020): ?>
    var labels2020 = [];
    var visitData2020 = [];

    // Obtener datos del año 2020
    <?php foreach ($dataForYear2020 as $contador): ?>
      labels2020.push('<?php echo $contador->Mes; ?>');
      visitData2020.push(<?php echo $contador->TotalVisitas; ?>);
    <?php endforeach; ?>

    var datos2020 = {
      labels: labels2020,
      datasets: [{
        label: 'Total Visits 2020',
        data: visitData2020,
        fill: false,
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 2
      }]
    };

    var opciones2020 = {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    var contexto2020 = document.getElementById('lineChart2020').getContext('2d');
    var graficoDeLinea2020 = new Chart(contexto2020, {
      type: 'line',
      data: datos2020,
      options: opciones2020
    });
  <?php else: ?>
    console.log('No hay datos disponibles para mostrar.');
  <?php endif; ?>
</script>


<!-- 2021 -->
<script>
  <?php if ($dataForYear2021): ?>
    var labels2020 = [];
    var visitData2020 = [];

    // Obtener datos del año 2020
    <?php foreach ($dataForYear2021 as $contador): ?>
      labels2020.push('<?php echo $contador->Mes; ?>');
      visitData2020.push(<?php echo $contador->TotalVisitas; ?>);
    <?php endforeach; ?>

    var datos2020 = {
      labels: labels2020,
      datasets: [{
        label: 'Total Visits 2021',
        data: visitData2020,
        fill: false,
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 2
      }]
    };

    var opciones2020 = {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    var contexto2020 = document.getElementById('lineChart2021').getContext('2d');
    var graficoDeLinea2020 = new Chart(contexto2020, {
      type: 'line',
      data: datos2020,
      options: opciones2020
    });
  <?php else: ?>
    console.log('No hay datos disponibles para mostrar.');
  <?php endif; ?>
</script>


<!-- 2022 -->
<script>
  <?php if ($dataForYear2022): ?>
    var labels2020 = [];
    var visitData2020 = [];

    // Obtener datos del año 2020
    <?php foreach ($dataForYear2022 as $contador): ?>
      labels2020.push('<?php echo $contador->Mes; ?>');
      visitData2020.push(<?php echo $contador->TotalVisitas; ?>);
    <?php endforeach; ?>

    var datos2020 = {
      labels: labels2020,
      datasets: [{
        label: 'Total Visits 2022',
        data: visitData2020,
        fill: false,
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 2
      }]
    };

    var opciones2020 = {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    var contexto2020 = document.getElementById('lineChart2022').getContext('2d');
    var graficoDeLinea2020 = new Chart(contexto2020, {
      type: 'line',
      data: datos2020,
      options: opciones2020
    });
  <?php else: ?>
    console.log('No hay datos disponibles para mostrar.');
  <?php endif; ?>
</script>


<!-- 2023 -->
<script>
  <?php if ($dataForYear2023): ?>
    var labels2020 = [];
    var visitData2020 = [];

    // Obtener datos del año 2020
    <?php foreach ($dataForYear2023 as $contador): ?>
      labels2020.push('<?php echo $contador->Mes; ?>');
      visitData2020.push(<?php echo $contador->TotalVisitas; ?>);
    <?php endforeach; ?>

    var datos2020 = {
      labels: labels2020,
      datasets: [{
        label: 'Total Visits 2023',
        data: visitData2020,
        fill: false,
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 2
      }]
    };

    var opciones2020 = {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    var contexto2020 = document.getElementById('lineChart2023').getContext('2d');
    var graficoDeLinea2020 = new Chart(contexto2020, {
      type: 'line',
      data: datos2020,
      options: opciones2020
    });
  <?php else: ?>
    console.log('No hay datos disponibles para mostrar.');
  <?php endif; ?>
</script>

<!-- ****************************************** SCRIPT ACTIVIDAD 4 *******************************************-->
<!-- LIZ -->


<!-- ****************************************** SCRIPT ACTIVIDAD 5 *******************************************-->
<!-- CINTIA -->


<!-- ****************************************** SCRIPT ACTIVIDAD 6 *******************************************-->
<!-- ANGEL -->